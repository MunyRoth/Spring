package com.munyroth.spring.app.requests;

import lombok.Builder;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

@Data
@Builder
public class PurchaseRequest implements Serializable {
    @Serial
    private static final long serialVersionUID = -6986746375915710855L;
    private Integer customer_id;
    private String coupon_code;
    private Float discount;
    private List<PurchaseDetail> purchase_details;

    @Data
    @Builder
    public static class PurchaseDetail {
        private Integer book_id;
        private Integer qty;
        private String edition;
    }
}
