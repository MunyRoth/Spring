package com.munyroth.spring.app.components;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

@Component
public class FileManager {
    @Scheduled(cron = "0 0 0 * */3 *") // this will run daily
    public void clearContent() throws IOException {
        FileWriter fwOb = new FileWriter("tmp.log", false);
        PrintWriter pwOb = new PrintWriter(fwOb, false);
        pwOb.flush();
        pwOb.close();
        fwOb.close();
    }
}
