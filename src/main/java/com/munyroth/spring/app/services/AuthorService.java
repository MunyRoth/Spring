package com.munyroth.spring.app.services;

import com.munyroth.spring.app.models.Author;
import com.munyroth.spring.app.repositories.AuthorRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AuthorService {
    private final AuthorRepository authorRepository;

    public Author addNewAuthor(Author author) {
        Optional<Author> authorByName = authorRepository.findByName(author.getName());
        if (authorByName.isPresent()) return null;
        return authorRepository.save(author);
    }

    public List<Author> getAllAuthors() {
        return authorRepository.findAll();
    }
    public Author getAuthor(Integer id) {
        Optional<Author> authorById = authorRepository.findById(id);
        return authorById.orElse(null);
    }

    @Transactional
    public Author updateAuthor(
            Integer id,
            String name,
            LocalDate dob,
            String gender,
            String email
    ) {
        Optional<Author> authorById = authorRepository.findById(id);
        if (authorById.isEmpty()) return null;

        Author author = authorById.get();
        if (name != null && !name.isEmpty() && !Objects.equals(author.getName(), name)) {
            author.setName(name);
        }
        if (dob != null && !Objects.equals(author.getDob(), dob)) {
            author.setDob(dob);
        }
        if (gender != null && !gender.isEmpty() && !Objects.equals(author.getGender(), gender)) {
            author.setGender(gender);
        }
        if (email != null && !email.isEmpty() && !Objects.equals(author.getEmail(), email)) {
            author.setEmail(email);
        }

        return author;
    }

    public Integer deleteAuthor(Integer id) {
        boolean exists = authorRepository.existsById(id);
        if (exists) {
            authorRepository.deleteById(id);
            return id;
        }
        return null;
    }
}
