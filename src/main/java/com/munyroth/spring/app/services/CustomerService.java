package com.munyroth.spring.app.services;

import com.munyroth.spring.app.models.Customer;
import com.munyroth.spring.app.repositories.CustomerRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CustomerService {
    private final CustomerRepository customerRepository;

    public boolean existsByEmail(String email) {
        return customerRepository.existsByEmail(email);
    }

    public Customer addCustomer(Customer customer) {
        Optional<Customer> customerByEmail = customerRepository.findByEmail(customer.getEmail());
        if (customerByEmail.isPresent()) return null;
        return customerRepository.save(customer);
    }

    public List<Customer> getAllCustomers() {
        return customerRepository.findAll();
    }
    public Customer getCustomer(Integer id) {
        Optional<Customer> customerById = customerRepository.findById(id);
        return customerById.orElse(null);
    }

    @Transactional
    public Customer updateCustomer(
            Integer id,
            String name,
            String email,
            LocalDate dob
    ) {
        Optional<Customer> customerById = customerRepository.findById(id);
        if (customerById.isEmpty()) return null;

        Customer customer = customerById.get();
        if (name != null && !name.isEmpty() && !Objects.equals(customer.getName(), name)) {
            customer.setName(name);
        }
        if (email != null && !email.isEmpty() && !Objects.equals(customer.getEmail(), email)) {
            customer.setEmail(email);
        }
        if (dob != null && !Objects.equals(customer.getDob(), dob)) {
            customer.setDob(dob);
        }
        return customer;
    }

    public Integer deleteCustomer(Integer id) {
        boolean exists = customerRepository.existsById(id);
        if (exists) {
            customerRepository.deleteById(id);
            return id;
        }
        return null;
    }
}
