package com.munyroth.spring.app.services;

import com.munyroth.spring.app.models.Author;
import com.munyroth.spring.app.models.Book;
import com.munyroth.spring.app.repositories.BookRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class BookService {
    private final BookRepository bookRepository;

    public Book addNewBook(Book book) {
        // check if book already exists by title
        Optional<Book> bookByTitle = bookRepository.findByTitle(book.getTitle());
        if (bookByTitle.isPresent()) return null;
        return bookRepository.save(book);
    }

    public List<Book> getAllBooks() {
        return bookRepository.findAll();
    }

    public List<Book> getBooksByAuthorId(Integer authorId) {
        Optional<List<Book>> booksByAuthorId = bookRepository.findByAuthorId(authorId);
        return booksByAuthorId.orElse(null);
    }

    public Book getBook(Integer id) {
        Optional<Book> bookById = bookRepository.findById(id);
        return bookById.orElse(null);
    }

    @Transactional
    public Book updateBook(
            Integer id,
            String title,
            Author author,
            Float price,
            Integer year
    ) {
        Optional<Book> bookById = bookRepository.findById(id);
        if (bookById.isEmpty()) return null;

        Book book = bookById.get();
        if (title != null && !title.isEmpty() && !Objects.equals(book.getTitle(), title)) {
            book.setTitle(title);
        }
        if (author != null && !Objects.equals(book.getAuthor(), author)) {
            book.setAuthor(author);
        }
        if (price != null && price != 0 && !Objects.equals(book.getPrice(), price)) {
            book.setPrice(price);
        }
        if (year != null && year != 0 && !Objects.equals(book.getYear(), year)) {
            book.setYear(year);
        }
        return book;
    }

    public Integer deleteBook(Integer id) {
        boolean exists = bookRepository.existsById(id);
        if (exists) {
            bookRepository.deleteById(id);
            return id;
        }
        return null;
    }
}
