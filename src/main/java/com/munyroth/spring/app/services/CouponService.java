package com.munyroth.spring.app.services;

import com.munyroth.spring.app.models.Coupon;
import com.munyroth.spring.app.repositories.CouponRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CouponService {
    private final CouponRepository couponRepository;

    public Coupon addCoupon(Coupon coupon) {
        return couponRepository.save(coupon);
    }

    public List<Coupon> getCouponsByCustomerId(Integer customerId) {
        return couponRepository.findAll();
    }
    public List<Coupon> filterByCreatedDate(LocalDate createdDate) {
        Optional<List<Coupon>> purchasesByCustomerId = couponRepository.findByCreatedDate(createdDate);
        return purchasesByCustomerId.orElse(null);
    }
    public List<Coupon> filterByUpdatedDate(LocalDate updatedDate) {
        Optional<List<Coupon>> purchasesByCustomerId = couponRepository.findByUpdatedDate(updatedDate);
        return purchasesByCustomerId.orElse(null);
    }
    public List<Coupon> filterByEffectiveDate(LocalDate effectiveDate) {
        Optional<List<Coupon>> purchasesByCustomerId = couponRepository.findByEffectDate(effectiveDate);
        return purchasesByCustomerId.orElse(null);
    }
    public List<Coupon> filterByExpiredDate(LocalDate expiredDate) {
        Optional<List<Coupon>> purchasesByCustomerId = couponRepository.findByExpiredDate(expiredDate);
        return purchasesByCustomerId.orElse(null);
    }
    public Coupon getCoupon(Integer id) {
        Optional<Coupon> purchaseById = couponRepository.findById(id);
        return purchaseById.orElse(null);
    }

    @Transactional
    public Coupon updateCoupon(
            Integer id,
            String code,
            Integer amount,
            Integer limit_count,
            LocalDate effect_date,
            LocalDate expired_date
    ) {
        Optional<Coupon> couponById = couponRepository.findById(id);
        if (couponById.isEmpty()) return null;

        Coupon coupon = couponById.get();
        if (code != null && !code.isEmpty() && !Objects.equals(coupon.getCode(), code)) {
            coupon.setCode(code);
        }
        if (amount != null && !Objects.equals(coupon.getAmount(), amount)) {
            coupon.setAmount(amount);
        }
        if (limit_count != null && !Objects.equals(coupon.getLimitCount(), limit_count)) {
            coupon.setLimitCount(limit_count);
        }
        if (effect_date != null && !Objects.equals(coupon.getEffectDate(), effect_date)) {
            coupon.setEffectDate(effect_date);
        }
        if (expired_date != null && !Objects.equals(coupon.getExpiredDate(), expired_date)) {
            coupon.setExpiredDate(expired_date);
        }
        return coupon;
    }

    public Integer deleteCoupon(Integer id) {
        boolean exists = couponRepository.existsById(id);
        if (exists) {
            couponRepository.deleteById(id);
            return id;
        }
        return null;
    }
}
