package com.munyroth.spring.app.services;

import com.munyroth.spring.app.models.PurchaseDetail;
import com.munyroth.spring.app.repositories.PurchaseDetailRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PurchaseDetailService {
    private final PurchaseDetailRepository purchaseDetailRepository;

    public PurchaseDetail addPurchaseDetail(PurchaseDetail purchaseDetail) {
        return purchaseDetailRepository.save(purchaseDetail);
    }
}
