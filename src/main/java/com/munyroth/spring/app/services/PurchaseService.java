package com.munyroth.spring.app.services;

import com.munyroth.spring.app.models.Coupon;
import com.munyroth.spring.app.models.Customer;
import com.munyroth.spring.app.models.Purchase;
import com.munyroth.spring.app.models.PurchaseDetail;
import com.munyroth.spring.app.repositories.CouponRepository;
import com.munyroth.spring.app.repositories.PurchaseRepository;
import com.munyroth.spring.app.requests.PurchaseRequest;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PurchaseService {
    private final PurchaseRepository purchaseRepository;
    private final CouponRepository couponRepository;

    private final BookService bookService;

    public Purchase addPurchase(Purchase purchase) {
        return purchaseRepository.save(purchase);
    }

    public Purchase addPurchase(Purchase purchase, String couponCode) {
        Coupon couponByCode = couponRepository.findByCode(couponCode).orElse(null);
        if (couponByCode != null && couponByCode.getLimitCount() > 0) {
            couponByCode.setLimitCount(couponByCode.getLimitCount()-1);
            purchase.setCoupon(couponByCode);
            purchase.setTotal_payable(purchase.getTotal_payable() - couponByCode.getAmount());
            return purchaseRepository.save(purchase);
        }
        else return null;
    }

    public List<Purchase> getPurchasesByCustomerId(Integer customerId) {
        Optional<List<Purchase>> purchasesByCustomerId = purchaseRepository.findByCustomerId(customerId);
        return purchasesByCustomerId.orElse(null);
    }
    public Purchase getPurchase(Integer id) {
        Optional<Purchase> purchaseById = purchaseRepository.findById(id);
        return purchaseById.orElse(null);
    }

    @Transactional
    public Purchase updatePurchase(
            Integer id,
            Customer customer,
            String coupon_code,
            List<PurchaseRequest.PurchaseDetail> purchaseDetails,
            Float discount
    ) {
        Optional<Purchase> purchaseById = purchaseRepository.findById(id);
        if (purchaseById.isEmpty()) return null;

        Purchase purchase = purchaseById.get();

        Float sub_total_price = sum(purchaseDetails, purchaseDetails.size());
        if (!purchaseDetails.isEmpty()) {
            purchase.getPurchase_details().clear();

            for (PurchaseRequest.PurchaseDetail purchaseDetail : purchaseDetails) {
                purchase.getPurchase_details().add(new PurchaseDetail(
                        purchase,
                        bookService.getBook(purchaseDetail.getBook_id()),
                        purchaseDetail.getQty(),
                        purchaseDetail.getEdition()));
            }
        }


        if (customer != null && !Objects.equals(purchase.getCustomer(), customer)) {
            purchase.setCustomer(customer);
        }
        // if change coupon, discount and sub-total
        if (discount != null
                && !Objects.equals(purchase.getDiscount(), discount)
                && !Objects.equals(purchase.getSub_total_price(), sub_total_price)
                && coupon_code != null
        ) {
            Coupon couponByCode = couponRepository.findByCode(coupon_code).orElse(null);
            if (couponByCode != null && couponByCode.getLimitCount() > 0) {
                couponByCode.setLimitCount(couponByCode.getLimitCount()-1);
                purchase.setCoupon(couponByCode);
                purchase.setSub_total_price(sub_total_price);
                purchase.setDiscount(discount);
                purchase.setTotal_payable(sub_total_price - discount - couponByCode.getAmount());
            }
        }
        // if change coupon and discount
        else if (discount != null
                && !Objects.equals(purchase.getDiscount(), discount)
                && coupon_code != null
        ) {
            Coupon couponByCode = couponRepository.findByCode(coupon_code).orElse(null);
            if (couponByCode != null && couponByCode.getLimitCount() > 0) {
                couponByCode.setLimitCount(couponByCode.getLimitCount()-1);
                purchase.setCoupon(couponByCode);
                purchase.setDiscount(discount);
                purchase.setTotal_payable(purchase.getSub_total_price() - discount - couponByCode.getAmount());
            }
        }
        // if change coupon and sub-total
        else if (coupon_code != null
                && !Objects.equals(purchase.getSub_total_price(), sub_total_price)
        ) {
            Coupon couponByCode = couponRepository.findByCode(coupon_code).orElse(null);
            if (couponByCode != null && couponByCode.getLimitCount() > 0) {
                couponByCode.setLimitCount(couponByCode.getLimitCount()-1);
                purchase.setCoupon(couponByCode);
                purchase.setSub_total_price(sub_total_price);
                purchase.setTotal_payable(sub_total_price - purchase.getDiscount() - couponByCode.getAmount());
            }
        }
        // if change discount and sub-total
        else if (discount != null
                && !Objects.equals(purchase.getDiscount(), discount)
                && !Objects.equals(purchase.getSub_total_price(), sub_total_price)
        ) {
            Coupon coupon = purchase.getCoupon();
            purchase.setSub_total_price(sub_total_price);
            purchase.setDiscount(discount);
            if (coupon != null) purchase.setTotal_payable(sub_total_price - discount - coupon.getAmount());
            else purchase.setTotal_payable(sub_total_price - discount);
        }
        // if change discount
        else if (discount != null
                && !Objects.equals(purchase.getDiscount(), discount)
        ) {
            purchase.setDiscount(discount);
            Coupon coupon = purchase.getCoupon();
            if (coupon != null) purchase.setTotal_payable(purchase.getSub_total_price() - discount - coupon.getAmount());
            else purchase.setTotal_payable(purchase.getSub_total_price() - discount);
        }
        // if change sub-total
        else if (sub_total_price != 0
                && !Objects.equals(purchase.getSub_total_price(), sub_total_price)
        ) {
            purchase.setSub_total_price(sub_total_price);
            Coupon coupon = purchase.getCoupon();
            if (coupon != null) purchase.setTotal_payable(sub_total_price - purchase.getDiscount() - coupon.getAmount());
            else purchase.setTotal_payable(sub_total_price - purchase.getDiscount());
        }
        // if change coupon code
        else if (coupon_code != null){
            Coupon couponByCode = couponRepository.findByCode(coupon_code).orElse(null);
            if (couponByCode != null && couponByCode.getLimitCount() > 0) {
                couponByCode.setLimitCount(couponByCode.getLimitCount()-1);
                purchase.setCoupon(couponByCode);
                purchase.setTotal_payable(purchase.getSub_total_price() - purchase.getDiscount() - couponByCode.getAmount());
            }
        }
        return purchase;
    }

    public Integer deletePurchase(Integer id) {
        boolean exists = purchaseRepository.existsById(id);
        if (exists) {
            purchaseRepository.deleteById(id);
            return id;
        }
        return null;
    }

    private Float sum(List<PurchaseRequest.PurchaseDetail> arr, int n) {
        if (n == 0) {
            return Float.parseFloat("0");
        } else {
            return bookService.getBook(arr.get(n - 1).getBook_id()).getPrice()*arr.get(n - 1).getQty() + sum(arr, n - 1);
        }
    }
}
