package com.munyroth.spring.app.services;

import com.munyroth.spring.app.models.CustomerAddress;
import com.munyroth.spring.app.repositories.CustomerAddressRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CustomerAddressService {
    private final CustomerAddressRepository customerAddressRepository;

    public CustomerAddress addCustomerAddress(CustomerAddress customerAddress) {
        return customerAddressRepository.save(customerAddress);
    }

    public List<CustomerAddress> getAllCustomerAddresses(Integer customerId) {
        Optional<List<CustomerAddress>> customerAddressByCustomerId = customerAddressRepository.findByCustomerId(customerId);
        return customerAddressByCustomerId.orElse(null);
    }
    public CustomerAddress getCustomerAddress(Integer id) {
        Optional<CustomerAddress> customerAddressById = customerAddressRepository.findById(id);
        return customerAddressById.orElse(null);
    }

    @Transactional
    public CustomerAddress updateCustomerAddress(
            Integer id,
            Integer zip_code,
            String city,
            String district,
            String commune,
            String home_number,
            String contact_number,
            String label
    ) {
        Optional<CustomerAddress> customerAddressById = customerAddressRepository.findById(id);
        if (customerAddressById.isEmpty()) return null;

        CustomerAddress customer = customerAddressById.get();
        if (zip_code != null && !Objects.equals(customer.getZipCode(), zip_code)) {
            customer.setZipCode(zip_code);
        }
        if (city != null && !city.isEmpty() && !Objects.equals(customer.getCity(), city)) {
            customer.setCity(city);
        }
        if (district != null && !Objects.equals(customer.getDistrict(), district)) {
            customer.setDistrict(district);
        }
        if (commune != null && !Objects.equals(customer.getCommune(), commune)) {
            customer.setCommune(commune);
        }
        if (home_number != null && !Objects.equals(customer.getHomeNumber(), home_number)) {
            customer.setHomeNumber(home_number);
        }
        if (contact_number != null && !Objects.equals(customer.getContactNumber(), contact_number)) {
            customer.setContactNumber(contact_number);
        }
        if (label != null && !Objects.equals(customer.getLabel(), label)) {
            customer.setLabel(label);
        }
        return customer;
    }

    public Integer deleteCustomerAddress(Integer id) {
        boolean exists = customerAddressRepository.existsById(id);
        if (exists) {
            customerAddressRepository.deleteById(id);
            return id;
        }
        return null;
    }
}
