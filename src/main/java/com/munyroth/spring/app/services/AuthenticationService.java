package com.munyroth.spring.app.services;

import com.munyroth.spring.app.models.Role;
import com.munyroth.spring.app.models.User;
import com.munyroth.spring.app.repositories.UserRepository;
import com.munyroth.spring.app.requests.LoginRequest;
import com.munyroth.spring.app.requests.RegisterRequest;
import com.munyroth.spring.security.oauth2.OauthService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AuthenticationService {

    private final AuthenticationManager authenticationManager;
    private final OauthService oauthService;
    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;

    public String registerAdmin(RegisterRequest request) {
        var user = User.builder()
                .firstname(request.getFirstname())
                .lastname(request.getLastname())
                .username(request.getUsername())
                .password(passwordEncoder.encode(request.getPassword()))
                .role(Role.ADMIN)
                .build();
        userRepository.save(user);

        return oauthService.generateToken(new UsernamePasswordAuthenticationToken(user.getUsername(), null, user.getAuthorities()));
    }

    public String register(RegisterRequest request) {
        Optional<User> userByUsername = userRepository.findByUsername(request.getUsername());
        if (userByUsername.isPresent()) return null;

        var user = User.builder()
                .firstname(request.getFirstname())
                .lastname(request.getLastname())
                .username(request.getUsername())
                .password(passwordEncoder.encode(request.getPassword()))
                .role(Role.USER)
                .build();
        userRepository.save(user);

        return oauthService.generateToken(new UsernamePasswordAuthenticationToken(user.getUsername(), null, user.getAuthorities()));
    }

    public String login(LoginRequest data) {
        var authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(data.getUsername(), data.getPassword()));
        return oauthService.generateToken(authentication);
    }

}
