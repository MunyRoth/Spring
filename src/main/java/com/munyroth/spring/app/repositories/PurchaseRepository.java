package com.munyroth.spring.app.repositories;

import com.munyroth.spring.app.models.Purchase;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface PurchaseRepository extends JpaRepository<Purchase, Integer>  {
    Optional<List<Purchase>> findByCustomerId(Integer customerId);
}