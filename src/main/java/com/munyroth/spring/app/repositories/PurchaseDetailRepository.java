package com.munyroth.spring.app.repositories;

import com.munyroth.spring.app.models.PurchaseDetail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PurchaseDetailRepository extends JpaRepository<PurchaseDetail, Integer> {
}
