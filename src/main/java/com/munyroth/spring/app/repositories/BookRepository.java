package com.munyroth.spring.app.repositories;

import com.munyroth.spring.app.models.Book;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface BookRepository extends JpaRepository<Book, Integer> {
    Optional<Book> findByTitle(String title);
    Optional<List<Book>> findByAuthorId(Integer authorId);
}