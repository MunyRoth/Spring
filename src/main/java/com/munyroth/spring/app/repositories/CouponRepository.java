package com.munyroth.spring.app.repositories;

import com.munyroth.spring.app.models.Coupon;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface CouponRepository extends JpaRepository<Coupon, Integer> {
    Optional<Coupon> findByCode(String couponCode);
    Optional<List<Coupon>> findByCreatedDate(LocalDate createdDate);
    Optional<List<Coupon>> findByUpdatedDate(LocalDate updatedDate);
    Optional<List<Coupon>> findByEffectDate(LocalDate effectiveDate);
    Optional<List<Coupon>> findByExpiredDate(LocalDate expiredDate);
}