package com.munyroth.spring.app.repositories;

import com.munyroth.spring.app.models.CustomerAddress;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CustomerAddressRepository extends JpaRepository<CustomerAddress, Integer> {
    Optional<List<CustomerAddress>> findByCustomerId(Integer customerId);
}