package com.munyroth.spring.app.controllers;

import com.munyroth.spring.app.requests.RegisterRequest;
import com.munyroth.spring.app.responses.ResponseHandler;
import com.munyroth.spring.app.services.AuthenticationService;
import com.munyroth.spring.app.requests.LoginRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/auth")
@RequiredArgsConstructor
public class AuthenticationController {

    private final AuthenticationService authenticationService;

    @PostMapping("/register")
    public ResponseEntity< Object> register(
            @RequestBody RegisterRequest request) {
        try {
            String token = authenticationService.register(request);
            if (token == null) return ResponseHandler.generateResponse(null, "username is already registered", HttpStatus.CONFLICT);

            Map<Object, Object> result = new HashMap<>();
            result.put("username", request.getUsername());
            result.put("token", token);
            return ResponseHandler.generateResponse(result, "registered successfully", HttpStatus.OK);
        } catch (AuthenticationException e) {
            return ResponseHandler.generateResponse(null, e.getMessage(), HttpStatus.MULTI_STATUS);
        }
    }

    @PostMapping("/login")
    public ResponseEntity< Object> login(
            @RequestBody LoginRequest request) {
        try {
            String token = authenticationService.login(request);
            Map<Object, Object> result = new HashMap<>();
            result.put("username", request.getUsername());
            result.put("token", token);
            return ResponseHandler.generateResponse(result, "logged in successfully", HttpStatus.OK);
        } catch (AuthenticationException e) {
            return ResponseHandler.generateResponse(null, e.getMessage(), HttpStatus.MULTI_STATUS);
        }
    }
}