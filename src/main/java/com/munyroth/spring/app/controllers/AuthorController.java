package com.munyroth.spring.app.controllers;

import com.munyroth.spring.app.models.Author;
import com.munyroth.spring.app.responses.ResponseHandler;
import com.munyroth.spring.app.services.AuthorService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@RestController
@RequestMapping("/api/authors")
@RequiredArgsConstructor
public class AuthorController {
    private final AuthorService authorService;
    @PostMapping
    public ResponseEntity<Object> addNewAuthor(
            @RequestParam String name,
            @RequestParam String dob,
            @RequestParam String gender,
            @RequestParam String email
    ) {
        try {
            // convert String dob to LocalDate dobParsed
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
            LocalDate dobParsed = LocalDate.parse(dob, formatter);

            // create and save author
            Author author = new Author(name, dobParsed, gender, email);
            Author result = authorService.addNewAuthor(author);

            if (result != null) return ResponseHandler.generateResponse(result, "added author successfully", HttpStatus.CREATED);
            else return ResponseHandler.generateResponse(null, "author is already exists", HttpStatus.CONFLICT);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(null, e.getMessage(), HttpStatus.MULTI_STATUS);
        }
    }

    @GetMapping
    public ResponseEntity<Object> getAllAuthors() {
        try {
            List<Author> result = authorService.getAllAuthors();
            return ResponseHandler.generateResponse(result, "retrieved authors successfully", HttpStatus.OK);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(null, e.getMessage(), HttpStatus.MULTI_STATUS);
        }
    }

    @GetMapping("{id}")
    public ResponseEntity<Object> getAuthor(
            @PathVariable("id") Integer id
    ) {
        try {
            Author author = authorService.getAuthor(id);
            if (author != null) return ResponseHandler.generateResponse(author, "retrieved author successfully", HttpStatus.OK);
            else return ResponseHandler.generateResponse(null, "not found", HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(null, e.getMessage(), HttpStatus.MULTI_STATUS);
        }
    }

    @PutMapping("{id}")
    public ResponseEntity<Object> updateAuthor(
            @PathVariable("id") Integer id,
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String dob,
            @RequestParam(required = false) String gender,
            @RequestParam(required = false) String email
    ) {
        try {
            // convert String dob to LocalDate dobParsed
            LocalDate dobParsed = null;
            if (dob != null) {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
                dobParsed = LocalDate.parse(dob, formatter);
            }

            Author author = authorService.updateAuthor(id, name, dobParsed, gender, email);
            if (author != null) return ResponseHandler.generateResponse(author, "updated author successfully", HttpStatus.OK);
            else return ResponseHandler.generateResponse(null, "not found", HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(null, e.getMessage(), HttpStatus.MULTI_STATUS);
        }
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Object> deleteAuthor(
            @PathVariable("id") Integer id
    ) {
        try {
            Integer result = authorService.deleteAuthor(id);
            if (result != null) return ResponseHandler.generateResponse(null, "deleted author successfully", HttpStatus.OK);
            else return ResponseHandler.generateResponse(null, "not found", HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(null, e.getMessage(), HttpStatus.MULTI_STATUS);
        }
    }
}
