package com.munyroth.spring.app.controllers;

import com.munyroth.spring.app.models.Customer;
import com.munyroth.spring.app.models.Purchase;
import com.munyroth.spring.app.models.PurchaseDetail;
import com.munyroth.spring.app.requests.PurchaseRequest;
import com.munyroth.spring.app.responses.ResponseHandler;
import com.munyroth.spring.app.services.BookService;
import com.munyroth.spring.app.services.CustomerService;
import com.munyroth.spring.app.services.PurchaseDetailService;
import com.munyroth.spring.app.services.PurchaseService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/purchases")
@RequiredArgsConstructor
public class PurchaseController {
    private final PurchaseService purchaseService;
    private final PurchaseDetailService purchaseDetailService;
    private final CustomerService customerService;
    private final BookService bookService;

    private Float sum(List<PurchaseRequest.PurchaseDetail> arr, int n) {
        if (n == 0) {
            return Float.parseFloat("0");
        } else {
            return bookService.getBook(arr.get(n - 1).getBook_id()).getPrice()*arr.get(n - 1).getQty() + sum(arr, n - 1);
        }
    }

    @PostMapping
    public ResponseEntity<Object> addNewPurchase(
            @RequestBody PurchaseRequest purchaseRequest
    ) {
        try {
            Customer customer = customerService.getCustomer(purchaseRequest.getCustomer_id());
            Purchase purchase;

            Float subTotalPrice = sum(purchaseRequest.getPurchase_details(), purchaseRequest.getPurchase_details().size());

            if (customer != null) {
                if (purchaseRequest.getDiscount() != null) purchase = new Purchase(customer, subTotalPrice, purchaseRequest.getDiscount(), subTotalPrice - purchaseRequest.getDiscount());
                else purchase = new Purchase(customer, subTotalPrice, Float.parseFloat("0"), subTotalPrice);
            }
            else return ResponseHandler.generateResponse(null, "customer does not exists", HttpStatus.NOT_FOUND);

            Purchase result;
            if (purchaseRequest.getCoupon_code() != null) {
                result = purchaseService.addPurchase(purchase, purchaseRequest.getCoupon_code());
            }
            else {
                result = purchaseService.addPurchase(purchase);
            }
            if (result != null) {
                PurchaseDetail purchaseDetail;
                for (int i = 0; i < purchaseRequest.getPurchase_details().size(); i++) {
                    purchaseDetail = new PurchaseDetail(
                            result,
                            bookService.getBook(purchaseRequest.getPurchase_details().get(i).getBook_id()),
                            purchaseRequest.getPurchase_details().get(i).getQty(),
                            purchaseRequest.getPurchase_details().get(i).getEdition());
                    result.getPurchase_details().add(purchaseDetailService.addPurchaseDetail(purchaseDetail));
                }
                return ResponseHandler.generateResponse(result, "added purchase successfully", HttpStatus.CREATED);
            }
            else return ResponseHandler.generateResponse(null, "coupon code is not exits", HttpStatus.CONFLICT);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(null, e.getMessage(), HttpStatus.MULTI_STATUS);
        }
    }

    @GetMapping("byCustomerId/{customerId}")
    public ResponseEntity<Object> getPurchasesByCustomerId(
            @PathVariable("customerId") Integer customerId
    ) {
        try {
            List<Purchase> result = purchaseService.getPurchasesByCustomerId(customerId);
            return ResponseHandler.generateResponse(result, "retrieved purchase successfully", HttpStatus.OK);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(null, e.getMessage(), HttpStatus.MULTI_STATUS);
        }
    }

    @GetMapping("{id}")
    public ResponseEntity<Object> getPurchase(
            @PathVariable("id") Integer id
    ) {
        try {
            Purchase purchase = purchaseService.getPurchase(id);
            if (purchase != null) return ResponseHandler.generateResponse(purchase, "retrieved purchase successfully", HttpStatus.OK);
            else return ResponseHandler.generateResponse(null, "not found", HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(null, e.getMessage(), HttpStatus.MULTI_STATUS);
        }
    }

    @PutMapping("{id}")
    public ResponseEntity<Object> updatePurchase(
            @PathVariable("id") Integer id,
            @RequestBody PurchaseRequest purchaseRequest
    ) {
        try {
            Customer customer = null;
            if (purchaseRequest.getCustomer_id() != null) customer = customerService.getCustomer(purchaseRequest.getCustomer_id());

            Purchase purchase = purchaseService.updatePurchase(id, customer, purchaseRequest.getCoupon_code(), purchaseRequest.getPurchase_details(), purchaseRequest.getDiscount());
            if (purchase != null) return ResponseHandler.generateResponse(purchase, "updated purchase successfully", HttpStatus.OK);
            else return ResponseHandler.generateResponse(null, "not found", HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(null, e.getMessage(), HttpStatus.MULTI_STATUS);
        }
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Object> deletePurchase(
            @PathVariable("id") Integer id
    ) {
        try {
            Integer result = purchaseService.deletePurchase(id);
            if (result != null) return ResponseHandler.generateResponse(null, "deleted purchase successfully", HttpStatus.OK);
            else return ResponseHandler.generateResponse(null, "not found", HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(null, e.getMessage(), HttpStatus.MULTI_STATUS);
        }
    }
}
