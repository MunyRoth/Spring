package com.munyroth.spring.app.controllers;

import com.munyroth.spring.app.models.Customer;
import com.munyroth.spring.app.models.CustomerAddress;
import com.munyroth.spring.app.responses.ResponseHandler;
import com.munyroth.spring.app.services.CustomerAddressService;
import com.munyroth.spring.app.services.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/customer_addresses")
@RequiredArgsConstructor
public class CustomerAddressController {
    private final CustomerAddressService customerAddressService;
    private final CustomerService customerService;

    @PostMapping
    public ResponseEntity<Object> addNewCustomerAddress(
            @RequestParam Integer customer_id,
            @RequestParam Integer zip_code,
            @RequestParam String city,
            @RequestParam String district,
            @RequestParam String commune,
            @RequestParam String home_number,
            @RequestParam String contact_number,
            @RequestParam String label
    ) {
        try {
            Customer customer = customerService.getCustomer(customer_id);
            CustomerAddress customerAddress;
            if (customer != null) customerAddress = new CustomerAddress(zip_code, city, district, commune, home_number, contact_number, label, customer);
            else return ResponseHandler.generateResponse(null, "customer does not exists", HttpStatus.NOT_FOUND);

            CustomerAddress result = customerAddressService.addCustomerAddress(customerAddress);
            if (result != null) return ResponseHandler.generateResponse(result, "added customerAddress successfully", HttpStatus.CREATED);
            else return ResponseHandler.generateResponse(null, "customerAddress is already exists", HttpStatus.CONFLICT);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(null, e.getMessage(), HttpStatus.MULTI_STATUS);
        }
    }

    @GetMapping("byCustomerId/{customerId}")
    public ResponseEntity<Object> getCustomerAddressesByCustomerId(
            @PathVariable("customerId") Integer customerId
    ) {
        try {
            List<CustomerAddress> result = customerAddressService.getAllCustomerAddresses(customerId);
            return ResponseHandler.generateResponse(result, "retrieved customer addresses successfully", HttpStatus.OK);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(null, e.getMessage(), HttpStatus.MULTI_STATUS);
        }
    }

    @PutMapping("{id}")
    public ResponseEntity<Object> updateCustomer(
            @PathVariable("id") Integer id,
            @RequestParam(required = false) Integer customer_id,
            @RequestParam(required = false) Integer zip_code,
            @RequestParam(required = false) String city,
            @RequestParam(required = false) String district,
            @RequestParam(required = false) String commune,
            @RequestParam(required = false) String home_number,
            @RequestParam(required = false) String contact_number,
            @RequestParam(required = false) String label
    ) {
        try {
            CustomerAddress customerAddress = customerAddressService.updateCustomerAddress(id, zip_code, city, district, commune, home_number, contact_number, label);
            if (customerAddress != null) return ResponseHandler.generateResponse(customerAddress, "updated customer address successfully", HttpStatus.OK);
            else return ResponseHandler.generateResponse(null, "not found", HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(null, e.getMessage(), HttpStatus.MULTI_STATUS);
        }
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Object> deleteCustomer(
            @PathVariable("id") Integer id
    ) {
        try {
            Integer result = customerAddressService.deleteCustomerAddress(id);
            if (result != null) return ResponseHandler.generateResponse(null, "deleted book successfully", HttpStatus.OK);
            else return ResponseHandler.generateResponse(null, "not found", HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(null, e.getMessage(), HttpStatus.MULTI_STATUS);
        }
    }
}
