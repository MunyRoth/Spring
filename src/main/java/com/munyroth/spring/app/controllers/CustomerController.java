package com.munyroth.spring.app.controllers;

import com.munyroth.spring.app.models.Customer;
import com.munyroth.spring.app.models.CustomerAddress;
import com.munyroth.spring.app.responses.ResponseHandler;
import com.munyroth.spring.app.services.CustomerAddressService;
import com.munyroth.spring.app.services.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@RestController
@RequestMapping("/api/customers")
@RequiredArgsConstructor
public class CustomerController {
    private final CustomerService customerService;
    private final CustomerAddressService customerAddressService;

    @PostMapping
    public ResponseEntity<Object> addNewCustomer(
            @RequestParam String name,
            @RequestParam String email,
            @RequestParam String dob,

            @RequestParam(required = false) Integer zip_code,
            @RequestParam(required = false) String city,
            @RequestParam(required = false) String district,
            @RequestParam(required = false) String commune,
            @RequestParam(required = false) String home_number,
            @RequestParam(required = false) String contact_number,
            @RequestParam(required = false) String label
    ) {
        try {
            // convert String dob to LocalDate dobParsed
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
            LocalDate dobParsed = LocalDate.parse(dob, formatter);

            boolean isExists = customerService.existsByEmail(email);
            if (!isExists) {
                Customer customer = new Customer(name, email, dobParsed);
                Customer result = customerService.addCustomer(customer);
                if (zip_code != null && city != null && district != null && commune != null && home_number != null && contact_number != null && label != null) {
                    // create customer address
                    CustomerAddress customerAddress = new CustomerAddress(zip_code, city, district, commune, home_number, contact_number, label, result);
                    result.getCustomerAddresses().add(customerAddressService.addCustomerAddress(customerAddress));
                }
                return ResponseHandler.generateResponse(result, "added customer successfully", HttpStatus.CREATED);
            }
            else return ResponseHandler.generateResponse(null, "customer is already exists", HttpStatus.CONFLICT);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(null, e.getMessage(), HttpStatus.MULTI_STATUS);
        }
    }

    @GetMapping
    public ResponseEntity<Object> getAllCustomers() {
        try {
            List<Customer> result = customerService.getAllCustomers();
            return ResponseHandler.generateResponse(result, "retrieved customer successfully", HttpStatus.OK);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(null, e.getMessage(), HttpStatus.MULTI_STATUS);
        }
    }

    @GetMapping("{id}")
    public ResponseEntity<Object> getCustomer(
            @PathVariable("id") Integer id
    ) {
        try {
            Customer customer = customerService.getCustomer(id);
            if (customer != null) return ResponseHandler.generateResponse(customer, "retrieved customer successfully", HttpStatus.OK);
            else return ResponseHandler.generateResponse(null, "not found", HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(null, e.getMessage(), HttpStatus.MULTI_STATUS);
        }
    }

    @PutMapping("{id}")
    public ResponseEntity<Object> updateCustomer(
            @PathVariable("id") Integer id,
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String email,
            @RequestParam(required = false) String dob
    ) {
        try {
            // convert String dob to LocalDate dobParsed
            LocalDate dobParsed = null;
            if (dob != null) {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
                dobParsed = LocalDate.parse(dob, formatter);
            }

            Customer customer = customerService.updateCustomer(id, name, email, dobParsed);
            if (customer != null) return ResponseHandler.generateResponse(customer, "updated customer successfully", HttpStatus.OK);
            else return ResponseHandler.generateResponse(null, "not found", HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(null, e.getMessage(), HttpStatus.MULTI_STATUS);
        }
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Object> deleteCustomer(
            @PathVariable("id") Integer id
    ) {
        try {
            Integer result = customerService.deleteCustomer(id);
            if (result != null) return ResponseHandler.generateResponse(null, "deleted customer successfully", HttpStatus.OK);
            else return ResponseHandler.generateResponse(null, "not found", HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(null, e.getMessage(), HttpStatus.MULTI_STATUS);
        }
    }
}
