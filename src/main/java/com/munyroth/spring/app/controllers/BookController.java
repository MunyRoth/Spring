package com.munyroth.spring.app.controllers;

import com.munyroth.spring.app.models.Author;
import com.munyroth.spring.app.responses.ResponseHandler;
import com.munyroth.spring.app.services.AuthorService;
import com.munyroth.spring.app.services.BookService;
import com.munyroth.spring.app.models.Book;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/books")
@RequiredArgsConstructor
public class BookController {
    private final BookService bookService;
    private final AuthorService authorService;

    @PostMapping
    public ResponseEntity<Object> addNewBook(
            @RequestParam String title,
            @RequestParam Integer author_id,
            @RequestParam Float price,
            @RequestParam Integer year
    ) {
        try {
            Author author = authorService.getAuthor(author_id);
            Book book;
            if (author != null) book = new Book(title, author, price, year);
            else return ResponseHandler.generateResponse(null, "author does not exists", HttpStatus.NOT_FOUND);

            Book result = bookService.addNewBook(book);
            if (result != null) return ResponseHandler.generateResponse(result, "added book successfully", HttpStatus.CREATED);
            else return ResponseHandler.generateResponse(null, "book is already exists", HttpStatus.CONFLICT);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(null, e.getMessage(), HttpStatus.MULTI_STATUS);
        }
    }

    @GetMapping
    public ResponseEntity<Object> getAllBooks() {
        try {
            List<Book> result = bookService.getAllBooks();
            return ResponseHandler.generateResponse(result, "retrieved books successfully", HttpStatus.OK);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(null, e.getMessage(), HttpStatus.MULTI_STATUS);
        }
    }

    @GetMapping("byAuthorId/{authorId}")
    public ResponseEntity<Object> getBooksByAuthorId(
            @PathVariable("authorId") Integer authorId
    ) {
        try {
            List<Book> result = bookService.getBooksByAuthorId(authorId);
            return ResponseHandler.generateResponse(result, "retrieved books successfully", HttpStatus.OK);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(null, e.getMessage(), HttpStatus.MULTI_STATUS);
        }
    }

    @GetMapping("{id}")
    public ResponseEntity<Object> getBook(
            @PathVariable("id") Integer id
    ) {
        try {
            Book book = bookService.getBook(id);
            if (book != null) return ResponseHandler.generateResponse(book, "retrieved book successfully", HttpStatus.OK);
            else return ResponseHandler.generateResponse(null, "not found", HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(null, e.getMessage(), HttpStatus.MULTI_STATUS);
        }
    }

    @PutMapping("{id}")
    public ResponseEntity<Object> updateBook(
            @PathVariable("id") Integer id,
            @RequestParam(required = false) String title,
            @RequestParam(required = false) Author author,
            @RequestParam(required = false) Float price,
            @RequestParam(required = false) Integer year
    ) {
        try {
            Book book = bookService.updateBook(id, title, author, price, year);
            if (book != null) return ResponseHandler.generateResponse(book, "updated book successfully", HttpStatus.OK);
            else return ResponseHandler.generateResponse(null, "not found", HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(null, e.getMessage(), HttpStatus.MULTI_STATUS);
        }
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Object> deleteBook(
            @PathVariable("id") Integer id
    ) {
        try {
            Integer result = bookService.deleteBook(id);
            if (result != null) return ResponseHandler.generateResponse(null, "deleted book successfully", HttpStatus.OK);
            else return ResponseHandler.generateResponse(null, "not found", HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(null, e.getMessage(), HttpStatus.MULTI_STATUS);
        }
    }
}