package com.munyroth.spring.app.controllers;

import com.munyroth.spring.app.models.Coupon;
import com.munyroth.spring.app.responses.ResponseHandler;
import com.munyroth.spring.app.services.CouponService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@RestController
@RequestMapping("/api/coupons")
@RequiredArgsConstructor
public class CouponController {
    private final CouponService couponService;
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");

    @PostMapping
    public ResponseEntity<Object> addNewCoupon(
            @RequestParam String code,
            @RequestParam Integer amount,
            @RequestParam Integer limit_count,
            @RequestParam String effect_date,
            @RequestParam String expired_date
    ) {
        try {
            LocalDate effectDateParsed = LocalDate.parse(effect_date, formatter);
            LocalDate expiredDateParsed = LocalDate.parse(expired_date, formatter);

            Coupon coupon = new Coupon(code, amount, limit_count, effectDateParsed, expiredDateParsed);
            Coupon result = couponService.addCoupon(coupon);
            if (result != null) return ResponseHandler.generateResponse(result, "added coupon successfully", HttpStatus.CREATED);
            else return ResponseHandler.generateResponse(null, "coupon is already exists", HttpStatus.CONFLICT);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(null, e.getMessage(), HttpStatus.MULTI_STATUS);
        }
    }

    @GetMapping("byCustomerId/{customerId}")
    public ResponseEntity<Object> getCouponsByCustomerId(
            @PathVariable("customerId") Integer customerId
    ) {
        try {
            List<Coupon> result = couponService.getCouponsByCustomerId(customerId);
            return ResponseHandler.generateResponse(result, "retrieved coupon successfully", HttpStatus.OK);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(null, e.getMessage(), HttpStatus.MULTI_STATUS);
        }
    }

    @GetMapping("byCreatedDate/{createdDate}")
    public ResponseEntity<Object> getCouponsByCreatedDate(
            @PathVariable("createdDate") String createdDate
    ) {
        try {
            // convert String createdDate to Date createdDateParsed
            LocalDate createdDateParsed = LocalDate.parse(createdDate, formatter);

            List<Coupon> result = couponService.filterByCreatedDate(createdDateParsed);
            return ResponseHandler.generateResponse(result, "retrieved coupon successfully", HttpStatus.OK);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(null, e.getMessage(), HttpStatus.MULTI_STATUS);
        }
    }
    @GetMapping("byUpdatedDate/{updatedDate}")
    public ResponseEntity<Object> getCouponsByUpdatedDate(
            @PathVariable("updatedDate") String updatedDate
    ) {
        try {
            // convert String createdDate to Date updatedDateParsed
            LocalDate updatedDateParsed = LocalDate.parse(updatedDate, formatter);

            List<Coupon> result = couponService.filterByUpdatedDate(updatedDateParsed);
            return ResponseHandler.generateResponse(result, "retrieved coupon successfully", HttpStatus.OK);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(null, e.getMessage(), HttpStatus.MULTI_STATUS);
        }
    }
    @GetMapping("byEffectiveDate/{effectiveDate}")
    public ResponseEntity<Object> getCouponsByEffectiveDate(
            @PathVariable("effectiveDate") String effectiveDate
    ) {
        try {
            // convert String effectiveDate to Date effectiveDateParsed
            LocalDate effectiveDateParsed = LocalDate.parse(effectiveDate, formatter);

            List<Coupon> result = couponService.filterByEffectiveDate(effectiveDateParsed);
            return ResponseHandler.generateResponse(result, "retrieved coupon successfully", HttpStatus.OK);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(null, e.getMessage(), HttpStatus.MULTI_STATUS);
        }
    }
    @GetMapping("byExpiredDate/{expiredDate}")
    public ResponseEntity<Object> getCouponsByExpiredDate(
            @PathVariable("expiredDate") String expiredDate
    ) {
        try {
            // convert String expiredDate to Date expiredDateParsed
            LocalDate expiredDateParsed = LocalDate.parse(expiredDate, formatter);

            List<Coupon> result = couponService.filterByExpiredDate(expiredDateParsed);
            return ResponseHandler.generateResponse(result, "retrieved coupon successfully", HttpStatus.OK);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(null, e.getMessage(), HttpStatus.MULTI_STATUS);
        }
    }

    @GetMapping("{id}")
    public ResponseEntity<Object> getCoupon(
            @PathVariable("id") Integer id
    ) {
        try {
            Coupon coupon = couponService.getCoupon(id);
            if (coupon != null) return ResponseHandler.generateResponse(coupon, "retrieved coupon successfully", HttpStatus.OK);
            else return ResponseHandler.generateResponse(null, "not found", HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(null, e.getMessage(), HttpStatus.MULTI_STATUS);
        }
    }

    @PutMapping("{id}")
    public ResponseEntity<Object> updateCoupon(
            @PathVariable("id") Integer id,
            @RequestParam(required = false) String code,
            @RequestParam(required = false) Integer amount,
            @RequestParam(required = false) Integer limit_count,
            @RequestParam(required = false) String effect_date,
            @RequestParam(required = false) String expired_date
    ) {
        try {
            // convert String effect_date to LocalDate effectDateParsed
            LocalDate effectDateParsed = null;
            if (effect_date != null) {
                effectDateParsed = LocalDate.parse(effect_date, formatter);
            }
            LocalDate expiredDateParsed = null;
            if (expired_date != null) {
                expiredDateParsed = LocalDate.parse(expired_date, formatter);
            }

            Coupon coupon = couponService.updateCoupon(id, code, amount, limit_count, effectDateParsed, expiredDateParsed);
            if (coupon != null) return ResponseHandler.generateResponse(coupon, "updated coupon successfully", HttpStatus.OK);
            else return ResponseHandler.generateResponse(null, "not found", HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(null, e.getMessage(), HttpStatus.MULTI_STATUS);
        }
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Object> deleteCoupon(
            @PathVariable("id") Integer id
    ) {
        try {
            Integer result = couponService.deleteCoupon(id);
            if (result != null) return ResponseHandler.generateResponse(null, "deleted coupon successfully", HttpStatus.OK);
            else return ResponseHandler.generateResponse(null, "not found", HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(null, e.getMessage(), HttpStatus.MULTI_STATUS);
        }
    }
}
