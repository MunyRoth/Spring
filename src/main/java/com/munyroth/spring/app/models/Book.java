package com.munyroth.spring.app.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Entity // This tells Hibernate to make a table out of this class
@Table(name = "books")
@Data
@NoArgsConstructor
public class Book implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    private String title;

    @Column(nullable = false)
    private Float price;

    @Column
    private Integer year;

    // mapping the id column of author table
    @ManyToOne
    @JoinColumn(name = "author_id", nullable = false)
    @JsonIgnoreProperties("books")
    private Author author;

    // Constructors
    public Book(String title, Author author, Float price, Integer year) {
        this.title = title;
        this.author = author;
        this.price = price;
        this.year = year;
    }
}
