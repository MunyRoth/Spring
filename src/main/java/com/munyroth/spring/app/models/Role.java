package com.munyroth.spring.app.models;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import static com.munyroth.spring.app.models.Permission.ADMIN_CREATE;
import static com.munyroth.spring.app.models.Permission.ADMIN_DELETE;
import static com.munyroth.spring.app.models.Permission.ADMIN_READ;
import static com.munyroth.spring.app.models.Permission.ADMIN_UPDATE;

@Getter
@RequiredArgsConstructor
public enum Role {

    USER(Collections.emptySet()),
    ADMIN(
            Set.of(
                    ADMIN_READ,
                    ADMIN_UPDATE,
                    ADMIN_DELETE,
                    ADMIN_CREATE
            )
    )
    ;

    private final Set<Permission> permissions;

    public List<SimpleGrantedAuthority> getAuthorities() {
        return Collections.singletonList(new SimpleGrantedAuthority(this.name()));
    }
}