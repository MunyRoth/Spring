package com.munyroth.spring.app.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "coupons")
@Data
@NoArgsConstructor
public class Coupon implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    private String code;

    @Column(nullable = false)
    private Integer amount;

    @Column(nullable = false)
    private Integer limitCount;

    @Column(nullable = false)
    private LocalDate effectDate;

    @Column(nullable = false)
    private LocalDate expiredDate;

    @CreationTimestamp
    private LocalDate createdDate;

    @UpdateTimestamp
    private LocalDate updatedDate;

    // mapping to purchase table
    @OneToMany(mappedBy = "coupon")
    @JsonIgnoreProperties({"coupon"})
    private List<Purchase> purchases;

    public Coupon(String code, Integer amount, Integer limitCount, LocalDate effectDate, LocalDate expiredDate) {
        this.code = code;
        this.amount = amount;
        this.limitCount = limitCount;
        this.effectDate = effectDate;
        this.expiredDate = expiredDate;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public void setLimitCount(Integer limit_count) {
        this.limitCount = limit_count;
    }

    public void setEffectDate(LocalDate effect_date) {
        this.effectDate = effect_date;
    }

    public void setExpiredDate(LocalDate expired_date) {
        this.expiredDate = expired_date;
    }
}
