package com.munyroth.spring.app.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "purchases")
@Data
@NoArgsConstructor
public class Purchase implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    // mapping to book table
    @OneToMany(mappedBy = "purchase", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<PurchaseDetail> purchase_details = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "customer_id", nullable = false)
    @JsonIgnoreProperties({"purchases", "customer_addresses"})
    private Customer customer;

    @ManyToOne
    @JoinColumn(name = "coupon_id")
    @JsonIgnoreProperties({"purchases", "limitCount"})
    private Coupon coupon;

    @Column(nullable = false)
    private Float sub_total_price;

    @Column
    private Float discount;

    @Column(nullable = false)
    private Float total_payable;

    public Purchase(Customer customer, Float sub_total_price, Float discount, Float total_payable) {
        this.customer = customer;
        this.sub_total_price = sub_total_price;
        this.discount = discount;
        this.total_payable = total_payable;
    }
}
