package com.munyroth.spring.app.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "purchase_details")
@Data
@NoArgsConstructor
@JsonIgnoreProperties("purchase")
public class PurchaseDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    // mapping the id column of purchase table
    @ManyToOne
    @JoinColumn(name = "purchase_id", nullable = false)
    private Purchase purchase;

    // mapping the id column of book table
    @ManyToOne
    @JoinColumn(name = "book_id", nullable = false)
    private Book book;

    @Column(nullable = false)
    private Integer qty;

    @Column(nullable = false)
    private String edition;

    public PurchaseDetail(Purchase purchase, Book book, Integer qty, String edition) {
        this.purchase = purchase;
        this.book = book;
        this.qty = qty;
        this.edition = edition;
    }
}