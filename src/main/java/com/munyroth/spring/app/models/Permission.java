package com.munyroth.spring.app.models;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum Permission {

    ADMIN_CREATE("ADMIN_CREATE"),
    ADMIN_READ("ADMIN_READ"),
    ADMIN_UPDATE("ADMIN_UPDATE"),
    ADMIN_DELETE("ADMIN_DELETE")
    ;

    private final String permission;
}