package com.munyroth.spring.app.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Entity
@Table(name = "customer_addresses")
@NoArgsConstructor
@JsonIgnoreProperties("customer")
public class CustomerAddress implements Serializable {
    @Getter
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    private Integer zip_code;

    @Column(nullable = false)
    private String city;

    @Column(nullable = false)
    private String district;

    @Column(nullable = false)
    private String commune;

    @Column
    private String home_number;

    @Column(nullable = false)
    private String contact_number;

    @Column
    private String label;

    // mapping the id column of author table
    @ManyToOne
    @JoinColumn(name = "customer_id", nullable = false)
    private Customer customer;

    public CustomerAddress(Integer zip_code, String city, String district, String commune, String home_number, String contact_number, String label) {
        this.zip_code = zip_code;
        this.city = city;
        this.district = district;
        this.commune = commune;
        this.home_number = home_number;
        this.contact_number = contact_number;
        this.label = label;
    }

    public CustomerAddress(Integer zip_code, String city, String district, String commune, String home_number, String contact_number, String label, Customer customer) {
        this.zip_code = zip_code;
        this.city = city;
        this.district = district;
        this.commune = commune;
        this.home_number = home_number;
        this.contact_number = contact_number;
        this.label = label;
        this.customer = customer;
    }

    public void setZipCode(Integer zip_code) {
        this.zip_code = zip_code;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public void setCommune(String commune) {
        this.commune = commune;
    }

    public void setHomeNumber(String home_number) {
        this.home_number = home_number;
    }

    public void setContactNumber(String contact_number) {
        this.contact_number = contact_number;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Integer getZipCode() {
        return zip_code;
    }

    public String getCity() {
        return city;
    }

    public String getDistrict() {
        return district;
    }

    public String getCommune() {
        return commune;
    }

    public String getHomeNumber() {
        return home_number;
    }

    public String getContactNumber() {
        return contact_number;
    }

    public String getLabel() {
        return label;
    }
}
