package com.munyroth.spring.config;

import com.munyroth.spring.security.rsa.RsaService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.security.KeyPair;
import java.security.KeyPairGenerator;

@Configuration
public class RsaConfig {
    @Bean
    public KeyPair keyPair() throws Exception {
        KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
        generator.initialize(512);
        return generator.generateKeyPair();
    }

    @Bean
    public RsaService rsa(KeyPair keyPair) {
        return new RsaService(keyPair.getPublic(), keyPair.getPrivate());
    }
}
