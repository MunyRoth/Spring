package com.munyroth.spring.config;

import com.munyroth.spring.security.rsa.RsaService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;
import java.nio.charset.StandardCharsets;

@Configuration
@RequiredArgsConstructor
public class DatabaseConfig {
    private final RsaService rsaService;
    private byte[] encUrlPos() throws Exception {
        return rsaService.encrypt("jdbc:postgresql://localhost:5433/postgres".getBytes());
    }
    private byte[] encUserPos() throws Exception {
        return rsaService.encrypt("postgres".getBytes());
    }
    private byte[] encPassPos() throws Exception {
        return rsaService.encrypt("734658".getBytes());
    }

    private byte[] encUrlMySql() throws Exception {
        return rsaService.encrypt("jdbc:mysql://localhost:3306/spring".getBytes());
    }
    private byte[] encUserMySql() throws Exception {
        return rsaService.encrypt("root".getBytes());
    }
    private byte[] encPassMySql() throws Exception {
        return rsaService.encrypt("73465826".getBytes());
    }

    private byte[] encUrlOracle() throws Exception {
        return rsaService.encrypt("jdbc:oracle:thin:@localhost:1521/xe".getBytes());
    }
    private byte[] encUserOracle() throws Exception {
        return rsaService.encrypt("system".getBytes());
    }
    private byte[] encPassOracle() throws Exception {
        return rsaService.encrypt("734658".getBytes());
    }


    @Primary
    @Bean
    public DataSource primaryDataSource() throws Exception {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUrl(new String(rsaService.decrypt(encUrlPos()), StandardCharsets.UTF_8));
        dataSource.setUsername(new String(rsaService.decrypt(encUserPos()), StandardCharsets.UTF_8));
        dataSource.setPassword(new String(rsaService.decrypt(encPassPos()), StandardCharsets.UTF_8));
        return dataSource;
    }

    @Bean
    public DataSource secondaryDataSource() throws Exception {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        dataSource.setUrl(new String(rsaService.decrypt(encUrlMySql()), StandardCharsets.UTF_8));
        dataSource.setUsername(new String(rsaService.decrypt(encUserMySql()), StandardCharsets.UTF_8));
        dataSource.setPassword(new String(rsaService.decrypt(encPassMySql()), StandardCharsets.UTF_8));
        return dataSource;
    }

    @Bean
    public DataSource tertiaryDataSource() throws Exception {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("oracle.jdbc.OracleDriver");
        dataSource.setUrl(new String(rsaService.decrypt(encUrlOracle()), StandardCharsets.UTF_8));
        dataSource.setUsername(new String(rsaService.decrypt(encUserOracle()), StandardCharsets.UTF_8));
        dataSource.setPassword(new String(rsaService.decrypt(encPassOracle()), StandardCharsets.UTF_8));
        return dataSource;
    }


}
